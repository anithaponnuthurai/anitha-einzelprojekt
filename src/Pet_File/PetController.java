package Pet_File;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;

import java.io.*;

public class PetController {
	private PetView view;
	private PetModel model;


	public PetController(PetModel model, PetView view) {
		this.model = model;
		this.view = view;

		view.btnAdd.setOnAction(this::add);
		view.btnDelete.setOnAction(this::delete);
		view.btnSave.setOnAction(this::save);
		view.select.setOnAction(this::click);
		view.write.setOnAction(this::write);
		view.read.setOnAction(this::read);


		view.btnDelete.disableProperty().bind(Bindings.isEmpty(model.pets()));
		view.btnSave.disableProperty().bind(Bindings.isEmpty(model.pets()));
		view.select.disableProperty().bind(Bindings.isEmpty(model.pets()));
		view.btnAdd.disableProperty().bind(view.txtName.textProperty().isEmpty());

	}

	public void numbers(){
		int i = view.petList.getItems().size() ;
		int j = view.petList.getSelectionModel().getSelectedIndex();
		String total = Integer.toString(i);
		String number = Integer.toString(view.petList.getSelectionModel().getSelectedIndex() +1 ) ;
		String combine = number + "/" + total;

		view.lblNumber.setText(combine);
	}

	private void add(ActionEvent e) {
		Pet.Species species = view.cmbSpecies.getValue();
		Pet.Gender gender = view.cmbGender.getValue();
		String name = view.txtName.getText();
		if (species != null && gender != null && name != null && name.length() > 0) {
			model.savePet(species, gender, name);
			updateView(model.getPet());
			model.pets().add(model.getPet());
			view.petList.setItems(model.pets());
		}
		view.txtName.clear();
		numbers();
	}

	public int getID(){
		int ID = view.petList.getSelectionModel().getSelectedItem().getID();
		return ID;
	}
	
	private void delete(ActionEvent e) {
		model.deletePet();
		updateView(model.getPet());
		Pet select = view.petList.getSelectionModel().getSelectedItem();
		model.pets().remove(select);
		numbers();
	}

	private void click(ActionEvent e){
		String name = view.petList.getSelectionModel().getSelectedItem().getName();
		Pet.Gender gender = view.petList.getSelectionModel().getSelectedItem().getGender();
		Pet.Species species = view.petList.getSelectionModel().getSelectedItem().getSpecies();
		if (species != null || gender != null || name != null || name.length() > 0) {
			updateSave(species,gender,name);
			numbers();
		}

	}

	private void save(ActionEvent e){
		Pet.Species species = view.cmbSpecies.getValue();
		Pet.Gender gender = view.cmbGender.getValue();
		String name = view.txtName.getText();

		refreshList(species, gender, name);
	}
	
	private void updateView(Pet pet) {
		if (pet != null) {
			view.lblDataID.setText(Integer.toString(pet.getID()));
			view.lblDataName.setText(pet.getName());
			view.lblDataSpecies.setText(pet.getSpecies().toString());
			view.lblDataGender.setText(pet.getGender().toString());
		} else {
			view.lblDataID.setText("");
			view.lblDataName.setText("");
			view.lblDataSpecies.setText("");
			view.lblDataGender.setText("");
		}
	}

	private void updateSave(Pet.Species Species, Pet.Gender Gender, String Name) {

		view.lblDataID.setText((Integer.toString(getID())));
		view.lblDataName.setText(Name);
		view.lblDataSpecies.setText(Species.toString());
		view.lblDataGender.setText(Gender.toString());

		view.txtName.setText(Name);
		view.cmbSpecies.setValue(Species);
		view.cmbGender.setValue(Gender);
	}

	private void refreshList(Pet.Species Species, Pet.Gender Gender, String Name) {
		int i = view.petList.getSelectionModel().getSelectedIndex();
		Pet pet = model.pets().get(i);

		pet.setName(Name);
		pet.setGender(Gender);
		pet.setSpecies(Species);
		view.petList.refresh();

		view.lblDataID.setText((Integer.toString(getID())));
		view.lblDataName.setText(Name);
		view.lblDataSpecies.setText(Species.toString());
		view.lblDataGender.setText(Gender.toString());
	}

	private void write(ActionEvent e){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("src/Pet_File/pets.txt"));
			for(int i = 0; i< model.pets().size(); i++){
				Pet pet = model.pets().get(i);
				String line = pet.getID() + ";" + pet.getName() + ";" + pet.getGender() + ";" + pet.getSpecies();
				bw.write(line);
				bw.newLine();
			}
			bw.close();
			System.out.println("Load war erfolgreich");
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public void read(ActionEvent e) {
		try {
			BufferedReader br = new BufferedReader(new FileReader("src/Pet_File/pets.txt"));
			String s;
			while ((s = br.readLine()) != null) {
				String[] pet = s.split(";");
				String ID = pet[0];
				String name = pet[1];
				Pet.Gender gender = Pet.Gender.valueOf(pet[2]);
				Pet.Species species = Pet.Species.valueOf(pet[3]);

				model.savePet(species, gender, name);
				model.pets().add(model.getPet());
				view.petList.setItems(model.pets());
			}
			br.close();
		} catch (FileNotFoundException ex) {
			throw new RuntimeException(ex);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

}
