package rechner;

public class rechnerModel {
    private rechnerView view;


    protected String calculate(String field) {

        boolean error = false;
        String result ="";
        //String zeichen = view.getField().getText();

        String[] strValues = field.split("[+,\\-,*,/]");

        int sum = 0;
        int differenc = 0;
        int product = 0;
        int division = 0;

        // Convert Strings to ints - catch any errors
        int[] intValues = new int[strValues.length];
        try { // parseInt may produce an exception
            for (int i = 0; i < strValues.length; i++) {
                intValues[i] = Integer.parseInt(strValues[i]);
            }
        } catch (NumberFormatException e) {
            error = true;
        }
        int value1 = intValues[0];
        int value2 = intValues[1];

        // If error, set result
        // If no error, sum all values and set result
        if (error) {
            result = "Error";
        } else if (field.contains("+")) {
            sum = value1 + value2;
            result = Integer.toString(sum);
        } else if (field.contains("-")) {
            differenc = value1 - value2;
            result = Integer.toString(differenc);
        } else if (field.contains("*")) {
            product = value1 * value2;
            result = Integer.toString(product);
        } else if (field.contains("/")) {
            division = value1 / value2;
            result = Integer.toString(division);
            }
            return result;
        }

}

