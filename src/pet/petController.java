package pet;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class petController {

    private petView view;
    private petModel model;


    public petController(petModel model, petView view) {
        this.model = model;
        this.view = view;

        view.btnAdd.setOnAction(this::add);
        view.btnDelete.setOnAction(this::delete);
        view.btnSave.setOnAction(this::save);
        view.select.setOnAction(this::click);

        view.btnDelete.disableProperty().bind(Bindings.isEmpty(model.pets()));
        view.btnSave.disableProperty().bind(Bindings.isEmpty(model.pets()));
        view.btnAdd.disableProperty().bind(view.txtName.textProperty().isEmpty());


        //view.btnForward.setOnAction(this::forward);
        //view.btnForward.setOnAction(this::backward);



    }

    public void numbers(){

    }

	/*public void forward(ActionEvent e){

	}*/

    /*public void backward(ActionEvent e){

        }
    }*/
    private void add(ActionEvent e) {
        Pet.Species species = view.cmbSpecies.getValue();
        Pet.Gender gender = view.cmbGender.getValue();
        String name = view.txtName.getText();
        if (species != null && gender != null && name != null && name.length() > 0) {
            model.savePet(species, gender, name);
            updateView(model.getPet());
            model.pets().add(model.getPet());
            view.petList.setItems(model.pets());
        }
        view.txtName.clear();
    }

    private void delete(ActionEvent e) {
        model.deletePet();
        updateView(model.getPet());
        Pet select = view.petList.getSelectionModel().getSelectedItem();
        model.pets().remove(select);
    }

    private void click(ActionEvent e){
        int Id = view.petList.getSelectionModel().getSelectedItem().getID();
        String name = view.petList.getSelectionModel().getSelectedItem().getName();
        Pet.Gender gender = view.petList.getSelectionModel().getSelectedItem().getGender();
        Pet.Species species = view.petList.getSelectionModel().getSelectedItem().getSpecies();
        if (species != null || gender != null || name != null || name.length() > 0) {
            model.savePet(species, gender, name);
            updateSave(model.getPet());
        }
    }

    private void save(ActionEvent e){
        Pet.Species species = view.cmbSpecies.getValue();
        Pet.Gender gender = view.cmbGender.getValue();
        String name = view.txtName.getText();
        if (species != null || gender != null || name != null || name.length() > 0) {
            //model.savePet(species, gender, name);
            model.petProperty();
            updateSave(model.getPet());
            view.petList.refresh();
        }
    }

    private void updateView(Pet pet) {
        if (pet != null) {
            view.lblDataID.setText(Integer.toString(pet.getID()));
            view.lblDataName.setText(pet.getName());
            view.lblDataSpecies.setText(pet.getSpecies().toString());
            view.lblDataGender.setText(pet.getGender().toString());
        } else {
            view.lblDataID.setText("");
            view.lblDataName.setText("");
            view.lblDataSpecies.setText("");
            view.lblDataGender.setText("");
        }
    }

    private void updateSave(Pet pet) {
        if (pet != null) {
            view.lblDataName.setText(pet.getName());
            view.lblDataSpecies.setText(pet.getSpecies().toString());
            view.lblDataGender.setText(pet.getGender().toString());
        }
    }



}



