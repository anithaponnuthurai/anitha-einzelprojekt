package pet;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class petView {
    private Stage stage;
    private petModel model;

    // Controls used for data processing
    TextField txtName = new TextField();
    ComboBox<Pet.Species> cmbSpecies = new ComboBox<>();
    ComboBox<Pet.Gender> cmbGender = new ComboBox<>();
    Label lblDataID = new Label();
    Label lblDataName = new Label();
    Label lblDataSpecies = new Label();
    Label lblDataGender = new Label();

    // Buttons
    Button btnSave = new Button("Save");
    Button btnDelete = new Button("Delete");
    Button btnAdd = new Button("Add");
    Button select = new Button("Select");

    Button btnForward = new Button(">");
    Button btnBackward = new Button("<");
    Label lblNumber = new Label();

    TableView<Pet> petList;
    TableColumn<Pet, String> nameTable = new TableColumn<>("Name");
    TableColumn<Pet, Pet.Species> speciesTableColumn = new TableColumn<>("Species");
    TableColumn<Pet, Pet.Gender> genderTableColumn = new TableColumn<>("Gender");
    //TableColumn<Pet,Integer> IDTableColumn = new TableColumn<>("ID");


    public petView(Stage stage, petModel model) {
        this.stage = stage;
        this.model = model;

        petList = new TableView<>(model.pets());

        //IDTableColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameTable.setCellValueFactory(new PropertyValueFactory<>("name"));
        speciesTableColumn.setCellValueFactory(new PropertyValueFactory<>("species"));
        genderTableColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));


        petList.getColumns().addAll(nameTable, speciesTableColumn, genderTableColumn);

        VBox root = new VBox();
        root.getChildren().add(createDataEntryPane());
        root.getChildren().add(createControlPane());
        root.getChildren().add(createListPane());
        root.getChildren().add(createDataDisplayPane());
        root.getChildren().add(petList);


        // Standard stuff for Scene and Stage
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("Pet.css").toExternalForm());
        stage.setTitle("Enter and display a pet");
        stage.setScene(scene);
        stage.show();
    }


    public void start() {
        stage.show();
    }


    private Pane createDataEntryPane() {
        GridPane pane = new GridPane();
        pane.setId("dataEntry");
        // Declare the individual controls in the GUI
        Label lblName = new Label("Name");
        Label lblSpecies = new Label("Species");
        Label lblGender = new Label("Gender");

        // Fill combos
        cmbSpecies.getItems().addAll(Pet.Species.values());
        cmbSpecies.setValue(Pet.Species.CAT);
        cmbGender.getItems().addAll(Pet.Gender.values());
        cmbGender.setValue(Pet.Gender.FEMALE);

        // Organize the layout, add in the controls (col, row)
        pane.add(lblName, 0, 0);		pane.add(txtName, 1, 0);
        pane.add(lblSpecies, 0, 1);		pane.add(cmbSpecies, 1, 1);
        pane.add(lblGender, 0, 2);	pane.add(cmbGender, 1, 2);

        return pane;
    }

    private Pane createControlPane() {
        GridPane pane = new GridPane();
        pane.setId("controlArea");
        pane.add(btnSave, 0, 0);
        pane.add(btnDelete, 1, 0);
        pane.add(btnAdd,2,0);
        pane.add(select,3,0);
        pane.setHgap(30);

        return pane;
    }

    private Pane createListPane(){
        GridPane pane = new GridPane();
        pane.setId("controlList");

        pane.add(btnBackward,0,0);
        pane.add(lblNumber,1,0);
        pane.add(btnForward,2,0);
        pane.setHgap(60);

        return pane;
    }

    private Pane createDataDisplayPane() {
        GridPane pane = new GridPane();
        pane.setId("dataDisplay");
        // Declare the individual controls in the GUI
        Label lblID = new Label("ID");
        Label lblName = new Label("Name");
        Label lblSpecies = new Label("Species");
        Label lblGender = new Label("Gender");

        // Organize the layout, add in the controls (col, row)
        pane.add(lblID, 0, 0); pane.add(lblDataID, 1, 0);
        pane.add(lblName, 0, 1); pane.add(lblDataName, 1, 1);
        pane.add(lblSpecies, 0, 2); pane.add(lblDataSpecies, 1, 2);
        pane.add(lblGender, 0, 3); pane.add(lblDataGender, 1, 3);


        return pane;
    }

}