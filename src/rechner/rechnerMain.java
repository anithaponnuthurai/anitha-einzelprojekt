package rechner;

import javafx.application.Application;
import javafx.stage.Stage;

public class rechnerMain extends Application{
    private rechnerView view;
    private rechnerModel model;
    private rechnerController controller;

    public void start(Stage primaryStage) throws Exception{
        model = new rechnerModel();
        view = new rechnerView(primaryStage, model);
        controller = new rechnerController(model, view);
        view.start();
    }
    public static void main(String[] args) {
        launch();
    }

}
