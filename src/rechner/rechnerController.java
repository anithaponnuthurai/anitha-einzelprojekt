package rechner;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class rechnerController {
        private rechnerView view;
        private rechnerModel model;

    public rechnerController(rechnerModel model, rechnerView view) {
        this.view = view;
        this.model = model;

            //Event-Handler für die Buttons
            for (Button button : view.getButtons()) {
                button.setOnAction(new ButtonClickHandler());
            }
        }

        // Event-Handler-Klasse für die Button-Klicks
        private class ButtonClickHandler implements EventHandler<ActionEvent> {
            @Override
            public void handle(ActionEvent event) {
                Button button = (Button) event.getSource();
                String buttonText = button.getText();

                if (buttonText.equals("=")) {
                    // Berechnung ausführen und das Ergebnis anzeigen
                    String expression = view.getField().getText();
                    String result = model.calculate(expression);
                    view.getField().setText(result);
                } else if (buttonText.equals("C")) {
                    // Textfeld löschen
                    view.getField().clear();
                } else {
                    // Textfeld um den Button-Text erweitern
                    view.getField().appendText(buttonText);
                }
            }
        }
}
