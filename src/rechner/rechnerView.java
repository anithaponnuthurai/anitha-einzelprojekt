package rechner;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class rechnerView {

    protected Stage stage;
    protected rechnerModel model;

    protected TextField field = new TextField();

    protected VBox layout = new VBox();

    protected GridPane calculator = new GridPane();


    public rechnerView(Stage primaryStage, rechnerModel model) {
        this.stage = primaryStage;
        this.model = model;

        BorderPane root = new BorderPane();


        String[][] buttonLabels = {
                {"7", "8", "9", "/"},
                {"4", "5", "6", "*"},
                {"1", "2", "3", "-"},
                {"C", "0", "=", "+"}
        };

        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                String label = buttonLabels[row][col];
                Button button = new Button(label);
                button.setMinSize(100, 100);

                calculator.add(button, col, row);

                root.setTop(field);
                root.setCenter(calculator);

                button.setOnAction(e -> field.appendText(label));
            }
        }

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("rechner.css").toExternalForm());
        stage.setTitle("Rechner");
        stage.setScene(scene);

    }

        public void start() {
            stage.show();
        }

    public TextField getField() {

        return field;
    }

    public Button[] getButtons() {
        Button[] buttons = new Button[calculator.getChildren().size()];
        calculator.getChildren().toArray(buttons);
        return buttons;
    }
}


