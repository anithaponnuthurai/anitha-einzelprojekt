package pet;

import javafx.application.Application;
import javafx.stage.Stage;

public class petMain extends Application {
    private petView view;
    private petModel model;
    private petController controller;


    @Override
    public void start(Stage primaryStage) throws Exception {
        model = new petModel();
        view = new petView(primaryStage, model);
        controller = new petController(model, view);
        view.start();
    }

    public static void main(String[] args) {
        launch();
    }
}
