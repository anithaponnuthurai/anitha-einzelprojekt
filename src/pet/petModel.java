package pet;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class petModel {

    private final SimpleObjectProperty<Pet> petProperty = new SimpleObjectProperty<>();

    private final ObservableList<Pet> pets = FXCollections.observableArrayList();


    public void savePet(Pet.Species species, Pet.Gender gender, String name) {
        petProperty.set(new Pet(species, gender, name));
    }

    public void deletePet() {
        petProperty.set(null);
    }

    public Pet getPet() {
        return petProperty.get();
    }


    public SimpleObjectProperty<Pet> petProperty() {
        return petProperty;
    }

    public ObservableList<Pet> pets(){
        return pets;
    }




}
